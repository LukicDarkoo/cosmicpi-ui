class Constants(object):
    allowed_events = ('on_temperature', 'on_pressure', 'on_magnetism')
    web_socket_port = 9000
    static_content_port = 8080