import threading
import os
import posixpath
import argparse
import urllib
import os

from SimpleHTTPServer import SimpleHTTPRequestHandler
from BaseHTTPServer import HTTPServer


class RootedHTTPServer(HTTPServer):
    def __init__(self, base_path, *args, **kwargs):
        HTTPServer.__init__(self, *args, **kwargs)
        self.RequestHandlerClass.base_path = base_path

class RootedHTTPRequestHandler(SimpleHTTPRequestHandler):
    def translate_path(self, path):
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = self.base_path
        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir):
                continue
            path = os.path.join(path, word)
        return path


class StaticContentServerHandler(object):
    def __init__(self, httpd):
        self._httpd = httpd

    def run(self):
        self._httpd.serve_forever()


class StaticContentServer(object):
    @staticmethod
    def async_start(port=8080, directory='web'):
        # Configure web directory
        web_dir = os.path.join(os.path.dirname(__file__), '../' + directory)

        # Start web server
        httpd = RootedHTTPServer(web_dir, ('', port), RootedHTTPRequestHandler)
        print("Serving an application at port:", port)

        # Start serving a content
        static_content_server_handler = StaticContentServerHandler(httpd=httpd)
        thread = threading.Thread(target=static_content_server_handler.run)
        thread.start()
